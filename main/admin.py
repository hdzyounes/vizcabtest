from django.contrib import admin

# Register your models here.

from .models import (
    Batiment,
    Element,
    Phase,
    PhaseElementImpact,
    Usage,
    Zone,
    ZoneElement,
)

admin.site.register(Batiment)
admin.site.register(Element)
admin.site.register(Phase)
admin.site.register(PhaseElementImpact)
admin.site.register(Usage)
admin.site.register(ZoneElement)


@admin.register(Zone)
class ZoneAdmin(admin.ModelAdmin):
    list_display = ("id", "nom", "surface", "usage", "batiment")
    list_filter = ("batiment", "usage")
    search_fields = ("nom", "batiment__nom", "usage__nom")
