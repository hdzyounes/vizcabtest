from main.serializers import BatimentSerializer
from rest_framework import generics
from main.models import Batiment


class BatimentDetail(generics.RetrieveAPIView):
    queryset = Batiment.objects.all()
    serializer_class = BatimentSerializer
