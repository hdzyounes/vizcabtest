from rest_framework import serializers

from main.models import Batiment


class BatimentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Batiment
        fields = ("id", "nom", "surface", "usage_finale", "impact")
