from django.db import models


class Batiment(models.Model):
    nom = models.CharField(max_length=255)
    periodeDeReference = models.IntegerField()
    usage = models.ForeignKey("Usage", null=True, blank=True, on_delete=models.PROTECT)

    @property
    def surface(self):
        return self.zones.aggregate(surface=models.Sum("surface"))["surface"]

    @property
    def usage_finale(self):
        usages = self.zones.values("usage").annotate(surface=models.Sum("surface"))
        if len(usages) == 1:
            return self.usage.nom
        else:
            usage = max(usages, key=lambda x: x["surface"])
            return Usage.objects.get(id=usage["usage"]).nom

    @property
    def impact(self):
        """
        J'ai volontairement simplifié le calcul pour zapper la formule d'exploitation
        """
        zone_elements = ZoneElement.objects.filter(zone__batiment=self)
        impact_total = 0
        for zone_element in zone_elements:
            element = zone_element.element
            quantity = zone_element.quantity
            impact = PhaseElementImpact.objects.filter(element=element).aggregate(
                impact=models.Sum("impact")
            )["impact"]
            impact_total += impact * quantity
        return impact_total

    def __str__(self):
        return self.nom + " - " + str(self.id)


class Zone(models.Model):
    nom = models.CharField(max_length=255)
    surface = models.FloatField()
    usage = models.ForeignKey("Usage", null=True, blank=True, on_delete=models.PROTECT)
    batiment = models.ForeignKey(
        "Batiment", related_name="zones", on_delete=models.CASCADE
    )
    elements = models.ManyToManyField("Element", through="ZoneElement")

    def __str__(self):
        return self.nom


class Element(models.Model):
    nom = models.CharField(max_length=255)
    unite = models.CharField(max_length=255)
    dureeVieTypique = models.IntegerField()

    def __str__(self):
        return self.nom


class Usage(models.Model):
    nom = models.CharField(max_length=255)

    def __str__(self):
        return self.nom + " - " + str(self.id)


class Phase(models.Model):
    nom = models.CharField(max_length=255)

    def __str__(self):
        return self.nom


class ZoneElement(models.Model):
    zone = models.ForeignKey("Zone", on_delete=models.CASCADE)
    element = models.ForeignKey("Element", on_delete=models.CASCADE)
    quantity = models.IntegerField()

    def __str__(self):
        return f"{self.zone} - {self.element} - {self.quantity}"


class PhaseElementImpact(models.Model):
    phase = models.ForeignKey("Phase", on_delete=models.CASCADE)
    element = models.ForeignKey("Element", on_delete=models.CASCADE)
    impact = models.FloatField()

    def __str__(self):
        return f"{self.element} - {self.phase} - {self.impact}"
