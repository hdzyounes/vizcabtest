from django.urls import path

from .views import BatimentDetail

urlpatterns = [
    path("batiments/<int:pk>/", BatimentDetail.as_view(), name="batiment-detail"),
]
