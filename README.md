
## Candidat
- Chaoui Younes

## Lancer le projet
```
git clone https://gitlab.com/vizcab_public/vizcab-backend-challenge.git
cd vizcabtest
docker compose up
```

## Peupler la base de données
```
docker compose exec vizcabtest-dev-api python manage.py migrate
docker compose exec vizcabtest-dev-api python manage.py loaddata usages.json batiments.json elements.json impacts.json phases.json  zoneelements.json zones.json
```

## Accéder à l'administration pour consulter les données
```
docker compose exec vizcabtest-dev-api python manage.py createsuperuser
```
Ensuite : http://127.0.0.1:8000/admin/

## Consulter une endpoint d'un batiment donné pour tester les résultats des 2 niveaux

http://127.0.0.1:8000/api/batiments/2/

## Lancer le projet en production
```
docker compose -f docker-compose-prod.yml up 
```
